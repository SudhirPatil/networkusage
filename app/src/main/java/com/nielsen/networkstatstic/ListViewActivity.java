package com.nielsen.networkstatstic;

import android.Manifest;
import android.app.AppOpsManager;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.widget.ListView;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.informate.odm.sdk.dualsim.ConnectivityInfo;
import com.informate.odm.sdk.managers.HardwareInfoManager;
import com.nielsen.networkstatstic.adapter.CustomListviewAdapter;
import com.nielsen.networkstatstic.adapter.ListModel;
import com.nielsen.networkstatstic.logger.Log;
import com.nielsen.networkstatstic.networkstatsmanager.NetworkStatsHelper;
import com.nielsen.networkstatstic.networkstatsmanager.module.DubaStatsModule;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkInterfaceModule;
import com.nielsen.networkstatstic.util.Util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {


    int[] ifaceList;

    private static final int PERMISSION_READ_STATE = 1012;
    private static final int PERMISSION_APP_USAGE = 1013;
    static Calendar startDate;
    static Calendar endDate;
    String[] subIds;
    NetworkStatsManager networkStatsManager;
    NetworkStatsHelper networkStatsHelper;
    ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        ifaceList = new int[]{ConnectivityManager.TYPE_MOBILE,
                ConnectivityManager.TYPE_BLUETOOTH,
                ConnectivityManager.TYPE_DUMMY,
                ConnectivityManager.TYPE_ETHERNET,
                ConnectivityManager.TYPE_MOBILE_DUN,
                ConnectivityManager.TYPE_VPN,
                ConnectivityManager.TYPE_WIFI,
                ConnectivityManager.TYPE_WIMAX};

        listview = (ListView) findViewById(R.id.listview);
        startDate = new GregorianCalendar();
        endDate = new GregorianCalendar();
        startDate.set(Calendar.HOUR, -2);
        networkStatsManager = (NetworkStatsManager) getApplicationContext().getSystemService(Context.NETWORK_STATS_SERVICE);
        networkStatsHelper = new NetworkStatsHelper(networkStatsManager, 0);
        HardwareInfoManager manager = new HardwareInfoManager(this);

        //Informate.enableODM(this,manager.getAndroidId(),"TEST001");
        if(isAllPermissionGranted()){
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String subid = tm.getSubscriberId();
            subIds =new String[]{subid};
            generateListData();

        }


//        if (ContextCompat.checkSelfPermission(ListViewActivity.this, Manifest.permission.PACKAGE_USAGE_STATS)
//                != PackageManager.PERMISSION_GRANTED) {
//            // We do not have this permission. Let's ask the user
//            subIds = new String[]{""};
//
//            ActivityCompat.requestPermissions(ListViewActivity.this, new String[]{Manifest.permission.PACKAGE_USAGE_STATS}, PERMISSION_READ_STATE);
//
//        }else {
//            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//            String subid = tm.getSubscriberId();
//            subIds =new String[]{subid};
//        }


    }

    public String getAdID(){
        try {
            AdvertisingIdClient.Info adInfo = AdvertisingIdClient.getAdvertisingIdInfo(this);
            String adId = adInfo != null ? adInfo.getId() : null;
            // Use the advertising id
            return adId;
        } catch (IOException | GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException exception) {
            // Error handling if needed
            return "";
        }
    }

    public boolean hasPermission(String permission){
        if (ContextCompat.checkSelfPermission(ListViewActivity.this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            // We do not have this permission. Let's ask the user
            subIds = new String[]{""};
            ActivityCompat.requestPermissions(ListViewActivity.this, new String[]{permission}, PERMISSION_READ_STATE);
            return false;

        }else {
            return true;
        }
    }

    public void generateListData(){

        TelephonyManager  tm = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        String subscriberID = tm.getSubscriberId();

        List<ListModel> modelList = new ArrayList<>();
        String subid ="";
        for (int index = 0; index < ifaceList.length; index++){
            if(ifaceList[index]==ConnectivityManager.TYPE_MOBILE){
                subid = subscriberID;
            }
            NetworkInterfaceModule model = networkStatsHelper.getInterfaceUsage(ifaceList[index],subid);
            if(model.getRx()>0 || model.getTx() >0){
                modelList.add(new ListModel(getDrawable(R.drawable.common_ic_googleplayservices),
                        model.getiFace(),"",
                        Util.getInstance().bytesIntoHumanReadable(model.totalUsage()),
                        Util.getInstance().bytesIntoHumanReadable(model.getRx()),
                        Util.getInstance().bytesIntoHumanReadable(model.getTx()),
                        "",""));
            }
        }
        List<DubaStatsModule> usage = networkStatsHelper.getDubaUsage(this, startDate.getTimeInMillis(), endDate.getTimeInMillis(), subIds);

        for(int index = 0; index < usage.size(); index++){
            DubaStatsModule usageInfo = usage.get(index);
            long mobileUsage = usageInfo.getMobileDownload() + usageInfo.getMobileUpdoad();
            long wifiUsage = usageInfo.getWifiDownload() + usageInfo.getWifiUpdoad();
            if((mobileUsage + wifiUsage )>0) {
                String totalUsage = Util.getInstance().bytesIntoHumanReadable((mobileUsage + wifiUsage));
                modelList.add(new ListModel(
                        usageInfo.getIcon(),
                        usageInfo.getName(),
                        usageInfo.getPackageName(),
                        totalUsage,
                        Util.getInstance().bytesIntoHumanReadable(usageInfo.getMobileDownload()),
                        Util.getInstance().bytesIntoHumanReadable(usageInfo.getMobileUpdoad()),
                        Util.getInstance().bytesIntoHumanReadable(usageInfo.getWifiDownload()),
                        Util.getInstance().bytesIntoHumanReadable(usageInfo.getWifiUpdoad())
                ));
            }
        }
        listview.setAdapter(new CustomListviewAdapter(ListViewActivity.this,modelList));
    }

    public boolean isAppUsageAccess(){
        try {
            AppOpsManager appOps = (AppOpsManager) this
                    .getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOps.checkOpNoThrow("android:get_usage_stats",
                    android.os.Process.myUid(), getPackageName());
            boolean granted = mode == AppOpsManager.MODE_ALLOWED;
            return granted;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  false;
    }

    public boolean isAllPermissionGranted(){
        if(!isAppUsageAccess()){
            requestAppUsageAccess();
        }else if(!hasPermission(Manifest.permission.READ_PHONE_STATE)){

        }else{
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_READ_STATE:
                if (grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted!
                    // you may now do the action that requires this permission
                    if(isAllPermissionGranted())
                        generateListData();
                } else {
                    // permission denied
                }
            case PERMISSION_APP_USAGE:
                if (grantResults.length > 0&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted!
                    // you may now do the action that requires this permission
                    if(isAllPermissionGranted())
                        generateListData();
                } else {
                    // permission denied
                }
                break;

        }
    }

    public void requestAppUsageAccess() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(this);
        builder.setTitle("Need Permission");
        builder.setMessage("Please allow App Usage permission");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                startActivityForResult(intent,PERMISSION_APP_USAGE);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isAllPermissionGranted()){
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String subid = tm.getSubscriberId();
            subIds =new String[]{subid};
            generateListData();

        }
    }
}
