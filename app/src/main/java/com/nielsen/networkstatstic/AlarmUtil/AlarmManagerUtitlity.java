package com.nielsen.networkstatstic.AlarmUtil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.nielsen.networkstatstic.receivers.LocalHourlyReceiver;

import java.util.Calendar;

/**
 * Created by patisu06 on 15-09-2016.
 */
public class AlarmManagerUtitlity {
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    public void setAlarm(Context context){

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, LocalHourlyReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

    // Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY, 8);
//        calendar.set(Calendar.MINUTE, 30);

// setRepeating() lets you specify a precise custom interval--in this case,
// 1 minute.
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 1, alarmIntent);
    }
}
