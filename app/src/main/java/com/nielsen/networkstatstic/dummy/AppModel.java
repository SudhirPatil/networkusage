package com.nielsen.networkstatstic.dummy;

import android.graphics.drawable.Drawable;

/**
 * Created by patisu06 on 12-10-2016.
 */

public class AppModel {

    public String appName;
    public int UID;

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public String packageName;

    public Drawable appIcon;

    public AppModel(int uid,String appName, String packageName,Drawable appIcon) {
        this.UID = uid;
        this.appName = appName;
        this.packageName = packageName;
        this.appIcon = appIcon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        this.appIcon = appIcon;
    }
}
