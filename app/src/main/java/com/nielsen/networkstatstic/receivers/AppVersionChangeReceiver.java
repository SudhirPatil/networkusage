package com.nielsen.networkstatstic.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.informate.odm.sdk.Informate;
import com.informate.odm.sdk.utils.UserProfileConfig;
import com.informate.odm.sdk.utils.Utility;

public class AppVersionChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            if (intent != null && intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
                if (intent.getData().getSchemeSpecificPart().equals(context.getPackageName())) { // Restart services.
                    String packageName = formatpackagename(intent.getData().toString());
                    if (packageName.equals(context.getPackageName())) {
                        Informate.enableODM(context, Utility.getAndroidId(context),"test_odm");
                    }
                }
            }
        } catch (Exception e) {
            Log.d("DD_update", e.getLocalizedMessage());
        }

    }



    private String formatpackagename(String data) {
        String packageName = data.substring(8, data.length());
        return packageName;

    }
}
