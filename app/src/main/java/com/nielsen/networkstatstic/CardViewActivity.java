package com.nielsen.networkstatstic;

import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.View;

import com.informate.odm.sdk.Informate;
import com.informate.odm.sdk.utils.Utility;
import com.nielsen.networkstatstic.adapter.CustomAdapter;
import com.nielsen.networkstatstic.networkstatsmanager.NetworkStatsHelper;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkStatsModule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by patisu06 on 27-09-2016.
 */
public class CardViewActivity extends AppCompatActivity {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<NetworkStatsModule> data;
    static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardviewlayout);

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        Informate.enableODM(this, Utility.getAndroidId(this),"test_odm");

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -12);
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String subid = tm.getSubscriberId();

        Calendar calCurrent = Calendar.getInstance();
        data = new ArrayList<NetworkStatsModule>();
        NetworkStatsManager networkStatsManager = (NetworkStatsManager) getApplicationContext().getSystemService(Context.NETWORK_STATS_SERVICE);
        NetworkStatsHelper networkStatsHelper = new NetworkStatsHelper(networkStatsManager, 0);
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_WIFI,cal.getTimeInMillis(),calCurrent.getTimeInMillis(),""));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_MOBILE,cal.getTimeInMillis(),System.currentTimeMillis(),subid));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_BLUETOOTH,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_DUMMY,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_ETHERNET,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_MOBILE_DUN,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_VPN,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));
        data.add(networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_WIMAX,cal.getTimeInMillis(),System.currentTimeMillis(),"0"));

        removedItems = new ArrayList<Integer>();

        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }

}
