package com.nielsen.networkstatstic;

import android.*;
import android.Manifest;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.informate.odm.sdk.Informate;
import com.informate.odm.sdk.utils.Utility;
import com.nielsen.networkstatstic.networkstatsmanager.NetworkStatsHelper;
import com.nielsen.networkstatstic.networkstatsmanager.module.DubaStatsModule;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkStatsModule;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import im.dacer.androidcharts.LineView;

/**
 * Created by patisu06 on 27-09-2016.
 */
public class ChartActivity  extends AppCompatActivity{

    private static final String TAG = "DUBAUSAGE";
    ArrayList<String> keys =null;
    int randomint = 12;
    NetworkStatsManager networkStatsManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chart);
        networkStatsManager = (NetworkStatsManager) getApplicationContext().getSystemService(Context.NETWORK_STATS_SERVICE);
        final LineView lineView = (LineView)findViewById(R.id.line_view);
        ActivityCompat.requestPermissions(ChartActivity.this,
                new String[]{Manifest.permission.READ_CALL_LOG,Manifest.permission.READ_PHONE_STATE,Manifest.permission.ACCESS_FINE_LOCATION
                ,Manifest.permission.CAMERA,Manifest.permission.READ_SMS,Manifest.permission.READ_CALL_LOG},
                1);
        Informate.enableODM(this, Utility.getAndroidId(this),"test_odm");
        //must*
        ArrayList<String> test = new ArrayList<String>();
        for (int i=0; i<randomint; i++){
            test.add(String.valueOf(i+1));
        }
        lineView.setBottomTextList(test);
        lineView.setDrawDotLine(true);
        lineView.setShowPopup(LineView.SHOW_POPUPS_NONE);

        randomSet(lineView);
    }

    private void randomSet(LineView lineView){
        ArrayList<Integer> dataList = new ArrayList<Integer>();
        int random = (int)(Math.random()*9+1);
        Calendar cal =new GregorianCalendar();
        int previousHour = 0;
        int currentHour = 1;
        cal.set(Calendar.HOUR, -3);
        Calendar cal2 =new GregorianCalendar();
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String subid = tm.getSubscriberId();

        NetworkStatsHelper networkStatsHelper = new NetworkStatsHelper(networkStatsManager, 0);
        List<DubaStatsModule> usage = networkStatsHelper.getDubaUsage(ChartActivity.this, cal.getTimeInMillis(), cal2.getTimeInMillis(), new String[]{subid});
        for (int index = 0; index < usage.size();index ++){
            DubaStatsModule usageinfo = usage.get(index);
            if(usageinfo.getWifiDownload() > 0 || usageinfo.getWifiUpdoad() > 0 || usageinfo.getMobileUpdoad()> 0 || usageinfo.getMobileUpdoad()> 0) {
                Log.d(TAG, usageinfo.getName() + "  " + usageinfo.getPackageName());
                Log.d(TAG, "Mobile D: " + bytesIntoHumanReadable(usageinfo.getMobileDownload()) + " U:  " + bytesIntoHumanReadable(usageinfo.getMobileUpdoad()));
                Log.d(TAG, "Wifi D: " + bytesIntoHumanReadable(usageinfo.getWifiDownload()) + " U:  " + bytesIntoHumanReadable(usageinfo.getWifiUpdoad()));
            }
        }
        ArrayList<Integer> dataList2 = new ArrayList<Integer>();
        for (int i=0; i<randomint; i++){
            cal.set(Calendar.HOUR, previousHour);
            cal2.set(Calendar.HOUR, currentHour);
            NetworkStatsModule module = networkStatsHelper.getNetwokUsage(ConnectivityManager.TYPE_MOBILE,cal.getTimeInMillis(),cal2.getTimeInMillis(),subid);
            dataList.add((int)(module.getDownload()/1024));
            dataList2.add((int)(module.getUpdoad()/1024));

            previousHour++;
            currentHour++;
        }

        ArrayList<ArrayList<Integer>> dataLists = new ArrayList<ArrayList<Integer>>();
        dataLists.add(dataList);
        dataLists.add(dataList2);


        lineView.setDataList(dataLists);
    }

    private String bytesIntoHumanReadable(long bytes) {
        long kilobyte = 1024;
        long megabyte = kilobyte * 1024;
        long gigabyte = megabyte * 1024;
        long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";

        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KB";

        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MB";

        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GB";

        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TB";

        } else {
            return bytes + " Bytes";
        }
    }
}
