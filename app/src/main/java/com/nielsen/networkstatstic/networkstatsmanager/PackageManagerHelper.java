package com.nielsen.networkstatstic.networkstatsmanager;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.nielsen.networkstatstic.MainActivity;
import com.nielsen.networkstatstic.dummy.AppModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robert Zagórski on 2016-09-09.
 */
public class PackageManagerHelper {


    public static boolean isPackage(Context context, CharSequence s) {
        PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo(s.toString(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static int getPackageUid(Context context, String packageName) {
        PackageManager packageManager = context.getPackageManager();
        int uid = -1;
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA);
            Log.d(MainActivity.class.getSimpleName(), packageInfo.packageName);
            uid = packageInfo.applicationInfo.uid;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return uid;
    }

    public static List<AppModel> getAppInfos(Context context){
        List<PackageInfo> packageInfoList = context.getPackageManager().getInstalledPackages(0);
        List<AppModel> modelList = new ArrayList<>();
        for (int i = 0; i < packageInfoList.size(); i++) {
            PackageInfo packageInfo = packageInfoList.get(i);
            String appName = packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString();
            String appVersion = packageInfo.versionName;
            int appVerCode = packageInfo.versionCode;
            Drawable app_icon = packageInfo.applicationInfo.loadIcon(context.getPackageManager());
            modelList.add(new AppModel(packageInfo.applicationInfo.uid,appName,packageInfo.packageName,app_icon));
        }
        return modelList;
    }


    public static List<ApplicationInfo> getInstalledApps(Context mContext){
        final PackageManager pm = mContext.getPackageManager();
//get a list of installed apps.
        return pm.getInstalledApplications(PackageManager.GET_META_DATA);
//
//        for (ApplicationInfo packageInfo : packages) {
//            Log.d(TAG, "Installed package :" + packageInfo.packageName);
//            Log.d(TAG, "Source dir : " + packageInfo.sourceDir);
//            Log.d(TAG, "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));
//        }



    }
}
