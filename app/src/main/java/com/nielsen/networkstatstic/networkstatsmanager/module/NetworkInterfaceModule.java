package com.nielsen.networkstatstic.networkstatsmanager.module;

/**
 * Created by patisu06 on 13-10-2016.
 */

public class NetworkInterfaceModule {

    String iFace;
    long rx;
    long tx;

    public String getiFace() {
        return iFace;
    }

    public long totalUsage(){
        return this.rx + this.tx;
    }

    public void setiFace(String iFace) {
        this.iFace = iFace;
    }

    public long getRx() {
        return rx;
    }

    public void setRx(long rx) {
        this.rx = rx;
    }

    public long getTx() {
        return tx;
    }

    public void setTx(long tx) {
        this.tx = tx;
    }
}
