package com.nielsen.networkstatstic.networkstatsmanager.module;

import android.graphics.drawable.Drawable;

/**
 * Created by patisu06 on 28-09-2016.
 */
public class DubaStatsModule {
    int UID;
    String name;
    String packageName;
    int tag;
    int state;
    int title;
    Drawable icon;
    String subid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public int getIsRoaming() {
        return isRoaming;
    }

    public void setIsRoaming(int isRoaming) {
        this.isRoaming = isRoaming;
    }

    public long getMobileDownload() {
        return mobileDownload;
    }

    public void setMobileDownload(long mobileDownload) {
        this.mobileDownload = mobileDownload;
    }

    public long getMobileUpdoad() {
        return mobileUpdoad;
    }

    public void setMobileUpdoad(long mobileUpdoad) {
        this.mobileUpdoad = mobileUpdoad;
    }

    public long getWifiDownload() {
        return wifiDownload;
    }

    public void setWifiDownload(long wifiDownload) {
        this.wifiDownload = wifiDownload;
    }

    public long getWifiUpdoad() {
        return wifiUpdoad;
    }

    public void setWifiUpdoad(long wifiUpdoad) {
        this.wifiUpdoad = wifiUpdoad;
    }

    int isRoaming;
    long mobileDownload;
    long mobileUpdoad;
    long wifiDownload;
    long wifiUpdoad;
}
