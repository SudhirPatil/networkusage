package com.nielsen.networkstatstic.networkstatsmanager.module;

/**
 * Created by patisu06 on 26-09-2016.
 */
public class NetworkStatsModule {
    int UID;
    int tag;
    int state;
    int title;
    int icon;

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getIsRoaming() {
        return isRoaming;
    }

    public void setIsRoaming(int isRoaming) {
        this.isRoaming = isRoaming;
    }

    public long getDownload() {
        return download;
    }

    public void setDownload(long download) {
        this.download = download;
    }

    public long getUpdoad() {
        return updoad;
    }

    public void setUpdoad(long updoad) {
        this.updoad = updoad;
    }

    int isRoaming;
    long download;
    long updoad;
}
