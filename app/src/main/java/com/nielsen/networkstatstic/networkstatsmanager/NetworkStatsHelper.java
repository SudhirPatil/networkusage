package com.nielsen.networkstatstic.networkstatsmanager;

import android.annotation.TargetApi;
import android.app.usage.NetworkStats;
import android.app.usage.NetworkStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.RemoteException;
import android.telephony.TelephonyManager;

import com.nielsen.networkstatstic.dummy.AppModel;
import com.nielsen.networkstatstic.networkstatsmanager.module.DubaStatsModule;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkInterfaceModule;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkStatsModule;
import com.nielsen.networkstatstic.util.Util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Robert Zagórski on 2016-09-09.
 */
@TargetApi(Build.VERSION_CODES.M)
public class NetworkStatsHelper {

    NetworkStatsManager networkStatsManager;
    int packageUid;

    public NetworkStatsHelper(NetworkStatsManager networkStatsManager) {
        this.networkStatsManager = networkStatsManager;
    }

    public NetworkStatsHelper(NetworkStatsManager networkStatsManager, int packageUid) {
        this.networkStatsManager = networkStatsManager;
        this.packageUid = packageUid;
    }

    public long getAllRxBytesMobile(Context context) {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis());
        } catch (RemoteException e) {
            return -1;
        }
        return bucket.getRxBytes();
    }

    public DubaStatsModule getWifiUsage(AppModel appInfo,long start,long end) {
        DubaStatsModule duba = new DubaStatsModule();
        duba.setUID(appInfo.getUID());
        duba.setIcon(appInfo.getAppIcon());
        duba.setPackageName(appInfo.getPackageName());
        duba.setName(appInfo.getAppName());
        NetworkStats networkStats = null;
        try {

            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_WIFI,
                    "",
                    start,
                    end,
                    appInfo.getUID());
            NetworkStats.Bucket bucket = new NetworkStats.Bucket();
            networkStats.getNextBucket(bucket);
            networkStats.getNextBucket(bucket);
            duba.setWifiDownload(bucket.getRxBytes());
            duba.setWifiUpdoad(bucket.getTxBytes());
        } catch (RemoteException e) {
            return duba;
        }
        return duba;
    }

    public DubaStatsModule getMobileUsage(AppModel appInfo,long start,long end,String subid){
        DubaStatsModule duba = new DubaStatsModule();
        duba.setUID(appInfo.getUID());
        duba.setPackageName(appInfo.getPackageName());
        duba.setName(appInfo.getAppName());
        duba.setSubid(subid);
        NetworkStats ns = null;
        try {
            ns = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_MOBILE,
                    subid,
                    start,
                    end,
                    appInfo.getUID());
            if(ns!=null) {
                NetworkStats.Bucket bucket = new NetworkStats.Bucket();
                ns.getNextBucket(bucket);
                //ns.getNextBucket(bucket);
                duba.setMobileDownload(bucket.getRxBytes());
                duba.setMobileUpdoad(bucket.getTxBytes());
            }
        } catch (RemoteException e) {
            return duba;
        }
        return duba;
    }

    public List<DubaStatsModule> getDubaUsage(Context context, long startdate, long enddate,String[] subids) {

        TelephonyManager  tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String subscriberID = tm.getSubscriberId();

        List<AppModel> appList = PackageManagerHelper.getAppInfos(context);
        List<DubaStatsModule> list = new ArrayList<>();
        for(int i = 0;i < appList.size(); i++) {
            DubaStatsModule totalUsage = getWifiUsage(appList.get(i), startdate, enddate);
            long mobileDownload = 0;
            long mobileUpload = 0;
            //list.add(getWifiUsage(context, appList.get(i), startdate, enddate));
            for (int index = 0; index < subids.length; index++) {
                DubaStatsModule mobile = (getMobileUsage(appList.get(i), startdate, enddate, subscriberID));
                mobileDownload += mobile.getMobileDownload();
                mobileUpload += mobile.getMobileUpdoad();
            }
            totalUsage.setMobileUpdoad(mobileUpload);
            totalUsage.setMobileDownload(mobileDownload);
            list.add(totalUsage);
        }
        return list;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public NetworkStatsModule getNetwokUsage(int type, long starttime, long endtime, String subid){
        NetworkStatsModule networkInfo = new NetworkStatsModule();
        Date d1 = new Date(starttime);
        Date d2 = new Date(endtime);
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(type,
                    subid,
                    d1.getTime(),
                    d2.getTime());
            networkInfo.setTitle(type);
            if(bucket !=null) {
                networkInfo.setDownload(bucket.getRxBytes());
                networkInfo.setUpdoad(bucket.getTxBytes());
                networkInfo.setIsRoaming(bucket.getRoaming());
                networkInfo.setState(bucket.getState());
            }
        } catch (RemoteException e) {
            return null;
        }
        return networkInfo;
    }

    public long getAllTxBytesMobile(Context context) {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis());
            if(bucket!=null)
                return bucket.getTxBytes();
            else
                return -1;
        } catch (RemoteException e) {
            return -1;
        }

    }

    public NetworkInterfaceModule getInterfaceUsage(int networktype,String subid) {
        NetworkStats.Bucket bucket;
        NetworkInterfaceModule ns = new NetworkInterfaceModule();
        try {
            bucket = networkStatsManager.querySummaryForDevice(networktype,
                    subid,
                    0,
                    System.currentTimeMillis());
            ns.setiFace(Util.getNetworkTypeName(networktype));
            if(bucket!=null) {
                ns.setRx(bucket.getRxBytes());
                ns.setTx(bucket.getTxBytes());
            }
        } catch (RemoteException e) {

        }
        return  ns;
    }

    public long getAllTxBytesWifi() {
        NetworkStats.Bucket bucket;
        try {
            bucket = networkStatsManager.querySummaryForDevice(ConnectivityManager.TYPE_VPN,
                    "",
                    0,
                    System.currentTimeMillis());
            if(bucket !=null){
                return bucket.getTxBytes();
            }else{
                return -1;
            }
        } catch (RemoteException e) {
            return -1;
        }

    }

    public long getPackageRxBytesMobile(Context context) {
        NetworkStats networkStats = null;
        try {

            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis(),
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        NetworkStats.Bucket bucket = new NetworkStats.Bucket();
        networkStats.getNextBucket(bucket);
        networkStats.getNextBucket(bucket);
        return bucket.getRxBytes();
    }

    public long getPackageTxBytesMobile(Context context) {
        NetworkStats networkStats = null;
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_MOBILE,
                    getSubscriberId(context, ConnectivityManager.TYPE_MOBILE),
                    0,
                    System.currentTimeMillis(),
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        NetworkStats.Bucket bucket = new NetworkStats.Bucket();
        networkStats.getNextBucket(bucket);
        return bucket.getTxBytes();
    }

    public long getPackageRxBytesWifi() {
        NetworkStats networkStats = null;
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_WIFI,
                    "",
                    0,
                    System.currentTimeMillis(),
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }
        NetworkStats.Bucket bucket = new NetworkStats.Bucket();
        networkStats.getNextBucket(bucket);
        return bucket.getRxBytes();
    }

    @TargetApi(Build.VERSION_CODES.N)
    public long getPackageTxBytesWifi() {
        NetworkStats networkStats = null;
        try {
            networkStats = networkStatsManager.queryDetailsForUid(
                    ConnectivityManager.TYPE_WIFI,
                    "",
                    0,
                    System.currentTimeMillis(),
                    packageUid);
        } catch (RemoteException e) {
            return -1;
        }

        NetworkStats.Bucket bucket = new NetworkStats.Bucket();
        networkStats.getNextBucket(bucket);

        return bucket.getTxBytes();
    }

    private String getSubscriberId(Context context, int networkType) {
        if (ConnectivityManager.TYPE_MOBILE == networkType) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            return tm.getSubscriberId();
        }
        return "";
    }


}
