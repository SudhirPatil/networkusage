package com.nielsen.networkstatstic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nielsen.networkstatstic.R;

import java.util.List;

/**
 * Created by patisu06 on 03-10-2016.
 */
public class CustomListviewAdapter extends ArrayAdapter<ListModel> {

    Context mContext;
    List<ListModel> mList;
    private static LayoutInflater inflater=null;

    public CustomListviewAdapter(Context context, List<ListModel> objects) {
        super(context, R.layout.list_row, objects);
        mList = objects;
        mContext = context;
        //inflater = ( LayoutInflater )mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


//    public CustomListviewAdapter(Context mContext, List<ListModel> list) {
//        // TODO Auto-generated constructor stub
//        mList = list;
//        mContext = mContext;
//        inflater = ( LayoutInflater )mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//    }

    @Override
    public int getCount() {
        return mList.size();
    }


    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.list_row, null);
        holder.txtAppName=(TextView) rowView.findViewById(R.id.appname);
        holder.txtPackageName=(TextView) rowView.findViewById(R.id.packagename);
        holder.txtTotalUsage=(TextView) rowView.findViewById(R.id.totalusage);
        holder.txtMobileUsage=(TextView) rowView.findViewById(R.id.mobileusage);
        holder.txtWifiUsage=(TextView) rowView.findViewById(R.id.wifiusage);
        holder.appIcon = (ImageView) rowView.findViewById(R.id.imageView);

        holder.txtAppName.setText(mList.get(position).getAppName());
        holder.txtPackageName.setText(mList.get(position).getPackageName());
        holder.txtTotalUsage.setText(mList.get(position).getTotalUsage());
        holder.txtMobileUsage.setText("Mobile : D :"+mList.get(position).getMobileDownload() +" U : "+mList.get(position).getMobileUpload());
        holder.txtWifiUsage.setText("Wifi : D : "+mList.get(position).getWifiDownload()+" U :"+mList.get(position).getWifiUpload());
        holder.appIcon.setImageDrawable(mList.get(position).getIcon());

        return rowView;
    }

    public class Holder
    {
        TextView txtAppName;
        TextView txtPackageName;
        TextView txtTotalUsage;
        TextView txtMobileUsage;
        TextView txtWifiUsage;
        ImageView appIcon;
    }
}
