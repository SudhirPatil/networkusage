package com.nielsen.networkstatstic.adapter;

import android.graphics.drawable.Drawable;

/**
 * Created by patisu06 on 03-10-2016.
 */
public class ListModel {

    Drawable icon;
    String appName;
    String packageName;
    String totalUsage;
    String mobileDownload;
    String mobileUpload;
    String wifiUpload;
    String wifiDownload;

    public ListModel(Drawable icon, String appName, String packageName, String totalUsage, String mobileDownload, String mobileUpload, String wifiUpload, String wifiDownload) {
        this.icon = icon;
        this.appName = appName;
        this.packageName = packageName;
        this.totalUsage = totalUsage;
        this.mobileDownload = mobileDownload;
        this.mobileUpload = mobileUpload;
        this.wifiUpload = wifiUpload;
        this.wifiDownload = wifiDownload;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(String totalUsage) {
        this.totalUsage = totalUsage;
    }

    public String getMobileDownload() {
        return mobileDownload;
    }

    public void setMobileDownload(String mobileDownload) {
        this.mobileDownload = mobileDownload;
    }

    public String getMobileUpload() {
        return mobileUpload;
    }

    public void setMobileUpload(String mobileUpload) {
        this.mobileUpload = mobileUpload;
    }

    public String getWifiUpload() {
        return wifiUpload;
    }

    public void setWifiUpload(String wifiUpload) {
        this.wifiUpload = wifiUpload;
    }

    public String getWifiDownload() {
        return wifiDownload;
    }

    public void setWifiDownload(String wifiDownload) {
        this.wifiDownload = wifiDownload;
    }
}
