package com.nielsen.networkstatstic.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nielsen.networkstatstic.CardViewActivity;
import com.nielsen.networkstatstic.R;
import com.nielsen.networkstatstic.networkstatsmanager.module.NetworkStatsModule;

import java.util.ArrayList;

/**
 * Created by patisu06 on 27-09-2016.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    public static final int TYPE_BLUETOOTH = 7;
    public static final int TYPE_DUMMY = 8;
    public static final int TYPE_ETHERNET = 9;
    public static final int TYPE_MOBILE = 0;
    public static final int TYPE_MOBILE_DUN = 4;
    /** @deprecated */
    @Deprecated
    public static final int TYPE_MOBILE_HIPRI = 5;
    /** @deprecated */
    @Deprecated
    public static final int TYPE_MOBILE_MMS = 2;
    /** @deprecated */
    @Deprecated
    public static final int TYPE_MOBILE_SUPL = 3;
    public static final int TYPE_VPN = 17;
    public static final int TYPE_WIFI = 1;
    public static final int TYPE_WIMAX = 6;

    private ArrayList<NetworkStatsModule> dataSet;

    public CustomAdapter(ArrayList<NetworkStatsModule> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cards_layout, parent, false);

        //view.setOnClickListener(CardViewActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    public String getTitle(int type){
        String networkType = "";
        switch (type){
            case TYPE_BLUETOOTH:
                networkType = "BLUETOOTH";
                break;
            case TYPE_DUMMY:
                networkType ="DUMMEY";
                break;
            case TYPE_ETHERNET:
                networkType ="ETHERNET";
                break;
            case TYPE_MOBILE:
                networkType ="MOBILE";
                break;
            case TYPE_MOBILE_DUN:
                networkType ="MOBILE_DUN";
                break;
            case TYPE_VPN:
                networkType ="VPN";
                break;
            case TYPE_WIFI:
                networkType ="WIFI";
                break;
            case TYPE_WIMAX:
                networkType ="WIMAX";
                break;
        }
        return networkType;
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView title = holder.txtTitle;
        TextView download = holder.txtDownload;
        TextView upload = holder.txtUpload;
        ImageView icon = holder.icIcon;

        title.setText(getTitle(dataSet.get(listPosition).getTitle()));
        download.setText(bytesIntoHumanReadable(dataSet.get(listPosition).getDownload()));
        upload.setText(bytesIntoHumanReadable(dataSet.get(listPosition).getUpdoad()));
        if(dataSet.get(listPosition).getIcon()>0)
            icon.setImageResource(dataSet.get(listPosition).getIcon());
    }



    private String bytesIntoHumanReadable(long bytes) {
        long kilobyte = 1024;
        long megabyte = kilobyte * 1024;
        long gigabyte = megabyte * 1024;
        long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";

        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KB";

        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MB";

        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GB";

        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TB";

        } else {
            return bytes + " Bytes";
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle;
        TextView txtDownload;
        TextView txtUpload;
        ImageView icIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
            this.txtDownload = (TextView) itemView.findViewById(R.id.txt_download);
            this.txtUpload = (TextView) itemView.findViewById(R.id.txt_upload);
            this.icIcon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }
}
